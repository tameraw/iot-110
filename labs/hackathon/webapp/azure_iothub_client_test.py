#!/usr/bin/python
import paho.mqtt.client as mqtt
import ssl

def on_connect(client, userdata, flags, rc):
    print "Connected with result code: %s" % rc
    client.subscribe("devices/TamerRPI3/messages/devicebound/#")

def on_disconnect(client, userdata, rc):
    print "Disconnected with result code: %s" % rc

def on_message(client, userdata, msg):
    print " - ".join((msg.topic, str(msg.payload)))
    # Do this only if you want to send a reply message every time you receive one
    client.publish("devices/TamerRPI3/messages/events/", "REPLY", qos=1)

def on_publish(client, userdata, mid):
    print "Sent message"

client = mqtt.Client(client_id="TamerRPI3", clean_session=True, userdata=None, protocol=mqtt.MQTTv311)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message
client.on_publish = on_publish
client.username_pw_set(username="tamer-iot-uw.azure-devices.net/TamerRPI3", password="SharedAccessSignature sr=tamer-iot-uw.azure-devices.net%2Fdevices%2FTamerRPI3&sig=F6OulN2KmpDiqZOZ0SsSzzJNCbxdXla2bn1NIz2tEiE%3D&se=1489984437")
#client.tls_insecure_set(True) # You can also set the proper certificate using client.tls_set()
client.tls_set("/etc/ssl/certs/ca-certificates.crt", certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect("tamer-iot-uw.azure-devices.net", port=8883)
client.loop_forever()
