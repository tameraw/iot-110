#!/usr/bin/python
import paho.mqtt.client as mqtt
import ssl
import time
import socket
import json
import random
from sense import PiSenseHat

# Azure IoT Hub - Device Connection Information
# iothubHostName      = "tamer-iot-uw.azure-devices.net"  # Azure IoT Hub Host Name
iothubHostName      = "tamermonitoringcd03f.azure-devices.net"  # Azure IoT Hub Host Name
iothubApiVersion    = "2016-11-14"                      # Azure IoT Hub API Version
deviceId            = "TamerRPI3"                       # DeviceID in Azure IoT Hub
# deviceSASToken      = "SharedAccessSignature sr=tamer-iot-uw.azure-devices.net%2Fdevices%2FTamerRPI3&sig=F6OulN2KmpDiqZOZ0SsSzzJNCbxdXla2bn1NIz2tEiE%3D&se=1489984437"
deviceSASToken      = "SharedAccessSignature sr=tamermonitoringcd03f.azure-devices.net%2Fdevices%2FTamerRPI3&sig=EUAghprh%2BxbshUgodA6DH3U9%2BYWQvEq9j4zAnQM1M1I%3D&se=1490430367"

# MQTT Connection Information
# Ref: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-mqtt-support
mqttBroker          = iothubHostName
mqttPort            = 8883                              # Secure Port
mqttClientID        = deviceId
mqttCleanSession    = True                              # 
mqttQoS             = 1                                 # 
mqttClientUserName  = iothubHostName+"/"+deviceId+"/api-version="+iothubApiVersion    # {iothubhostname}/{device_id}/api-version=2016-11-14
mqttClientPassword  = deviceSASToken                                            # SharedAccessSignature sig={signature-string}&se={expiry}&sr={URL-encoded-resourceURI}
deviceToCloudTopic  = "devices/"+deviceId+"/messages/events/"                   # D2C: For sending device to cloud messages
cloudToDeviceTopic  = "devices/"+deviceId+"/messages/devicebound/#"             # C2D: For device to receive messages from the cloud
caCertPath          = "/etc/ssl/certs/ca-certificates.crt"                      # Install: sudo apt-get install ca-certificates
tlsVersion          = ssl.PROTOCOL_TLSv1_2                                      # The version of the SSL/TLS protocol to be used

# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print "Connected with result code: %s" % rc
    client.subscribe(cloudToDeviceTopic, qos=mqttQoS)

# The callback for when a PUBLISH message is received from the broker.
def on_disconnect(client, userdata, rc):
    client.unsubscribe(cloudToDeviceTopic)
    print "Disconnected with result code: %s" % rc

line = 0

def on_message(client, userdata, msg):
    global line
    print "Message received from Cloud"
    print "Msg Topic   ==> " + msg.topic
    print "Msg payload ==> " + str(msg.payload)
    print "UserData ==> " + str(userdata)

    jsonMsg = json.loads(msg.payload)
    # if line > 9:
    #     line = 0
    if jsonMsg['Name'] == "displayLine" :
        displayLine(jsonMsg['Parameters']['row'])
    # line += 1

    # Do this only if you want to send a reply message every time you receive one
    client.publish(deviceToCloudTopic, "REPLY=ACK", qos=mqttQoS)

def on_publish(client, userdata, mid):
    print "Sent message"

client = mqtt.Client(client_id=mqttClientID, clean_session=mqttCleanSession, userdata=None, protocol=mqtt.MQTTv311)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message
client.on_publish = on_publish

client.username_pw_set(username=mqttClientUserName, password=mqttClientPassword)
client.tls_set(caCertPath, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=tlsVersion, ciphers=None)
client.connect(mqttBroker, port=mqttPort)

# =========================== Device Model ============================================== #
deviceMetaData = {
  'ObjectType': 'DeviceInfo',
  'IsSimulatedDevice': 0,
  'Version': '1.0',
  'DeviceProperties': {
    'DeviceID': deviceId,
    'HubEnabledState': 1,
    'CreatedTime': '2015-09-21T20:28:55.5448990Z',
    'DeviceState': 'normal',
    'UpdatedTime': "",
    'Manufacturer': 'Contoso Inc.',
    'ModelNumber': 'MD-909',
    'SerialNumber': 'SER9090',
    'FirmwareVersion': '1.10',
    'Platform': 'node.js',
    'Processor': 'ARM',
    'InstalledRAM': '64 MB',
    'Latitude': 47.617025,
    'Longitude': -122.191285
  },
  'Commands':
  [
      {
          'Name': 'SetTemperature',
          'Parameters': [
              {
                  'Name': 'Temperature',
                  'Type': 'double'
              }
          ]
      },
      {
          'Name': 'displayLine',
          'Parameters': [
              {
                  'Name': 'row',
                  'Type': 'int'
              }
          ]
      },      
  ],
  'Telemetry':[
      {
          'Name': 'Temperature',
          'Type': 'double',
          'DisplayName': 'Temp(C*)'
      },
      {
          'Name': 'Humidity',
          'Type': 'double',
          'DisplayName': 'Humidity'
      },
      {
        'Name': 'Motion',
        'Type': 'int',
        'DisplayName': 'Motion'
      },
      {
        'Name': 'Gas',
        'Type': 'int',
        'DisplayName': 'Gas'
      }
  ]
};

# =========================== Device Model ============================================== #

# =========================== Utility Functions ========================================= #
# display one row of blue LEDs (just for fun)
def displayLine(row):
    if row > -1 and row < 8:
        blue = (0, 0, 255)
        for i in range(0,8):
            x = i % 8
            y = (i / 8) + row
            # print("set pixel x:%d y:%d" % (x,y))
            pi_sense.set_pixel(x,y,blue)
            time.sleep(0.02)
    else:
        print "ERROR: Invalid row value: " + str(row)
        print "Row value must be between 0 and 7 inclusive"

# Get hostname
# import pdb; pdb.set_trace()
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]
# =========================== Utility Functions ========================================= #

# Create a sense-hat object
pi_sense = PiSenseHat()
print('PI SenseHat Object Created')

pi_sense.clear_display()

# initialize message dictionary - Not needed
msg = {'topic':deviceToCloudTopic, 'payload':"", 'qos':mqttQoS }

client.publish(deviceToCloudTopic, str(deviceMetaData), qos=mqttQoS)
time.sleep(2.0)

print('Getting Sensor Data')
for i in range(1,100):
    print("SensorSet[%d]" % (i))
    sensors = pi_sense.getAllSensors()

    sensors['host'] = hostname
    msg['payload'] = str(sensors)
    #print("msg["+str(i)+"]:"+msg['payload'])
    #client.publish(deviceToCloudTopic, msg['payload'], qos=mqttQoS)
    
    data = {
        'DeviceID': deviceId,
        'Temperature': pi_sense.getTemperature(),
        'Humidity': pi_sense.getHumidity(),
        'Motion': random.randrange(0, 2),
        'Gas': random.randrange(0,100)
    }
    print("data["+str(i)+"]"+str(data))
    client.publish(deviceToCloudTopic, str(data), qos=mqttQoS)
    time.sleep(2.0)

client.loop_forever()

quit()
